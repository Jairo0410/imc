var ctx = document.getElementById("lineChart").getContext('2d');
var myChart = new Chart(ctx, {
 type: 'line',
  data: {
     labels: labels,
   datasets: [
     {
      label: "IMC per date",
      backgroundColor: "rgba(155, 89, 182,0.2)",
      borderColor: "rgba(142, 68, 173,1.0)",
      pointBackgroundColor: "rgba(142, 68, 173,1.0)",
      data: data
     }
    ]
  },
  options: {
   scales: {
    yAxes: [{
     ticks: {
      beginAtZero:true
     }
    }]
   }
  }
});