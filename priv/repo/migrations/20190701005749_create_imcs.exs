defmodule Imc.Repo.Migrations.CreateImcs do
  use Ecto.Migration

  def change do
    create table(:imcs, primary_key: false) do
      add :fecha, :date, primary_key: true
      add :altura_cm, :float
      add :peso_lb, :float
    end

  end
end
