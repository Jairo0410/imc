defmodule Imc.Repo do
  use Ecto.Repo,
    otp_app: :imc,
    adapter: Ecto.Adapters.Postgres
end
