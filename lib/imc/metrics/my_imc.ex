defmodule Imc.Metrics.MyIMC do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:fecha, :date, []}
  @derive {Phoenix.Param, key: :fecha}
  schema "imcs" do
    field :altura_cm, :float
    field :peso_lb, :float
  end

  @doc false
  def changeset(my_imc, attrs) do
    my_imc
    |> cast(attrs, [:fecha, :altura_cm, :peso_lb])
    |> validate_required([:fecha, :altura_cm, :peso_lb])
  end
end
