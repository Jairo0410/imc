defmodule Imc.Metrics do
  @moduledoc """
  The Metrics context.
  """

  import Ecto.Query, warn: false
  alias Imc.Repo

  alias Imc.Metrics.MyIMC

  @doc """
  Returns the list of imcs.

  ## Examples

      iex> list_imcs()
      [%MyIMC{}, ...]

  """
  def list_imcs do
    Repo.all(MyIMC)
  end

  @doc """
  Gets a single my_imc.

  Raises `Ecto.NoResultsError` if the My imc does not exist.

  ## Examples

      iex> get_my_imc!(123)
      %MyIMC{}

      iex> get_my_imc!(456)
      ** (Ecto.NoResultsError)

  """
  def get_my_imc!(id), do: Repo.get!(MyIMC, id)

  @doc """
  Creates a my_imc.

  ## Examples

      iex> create_my_imc(%{field: value})
      {:ok, %MyIMC{}}

      iex> create_my_imc(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_my_imc(attrs \\ %{}) do
    %MyIMC{}
    |> MyIMC.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a my_imc.

  ## Examples

      iex> update_my_imc(my_imc, %{field: new_value})
      {:ok, %MyIMC{}}

      iex> update_my_imc(my_imc, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_my_imc(%MyIMC{} = my_imc, attrs) do
    my_imc
    |> MyIMC.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a MyIMC.

  ## Examples

      iex> delete_my_imc(my_imc)
      {:ok, %MyIMC{}}

      iex> delete_my_imc(my_imc)
      {:error, %Ecto.Changeset{}}

  """
  def delete_my_imc(%MyIMC{} = my_imc) do
    Repo.delete(my_imc)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking my_imc changes.

  ## Examples

      iex> change_my_imc(my_imc)
      %Ecto.Changeset{source: %MyIMC{}}

  """
  def change_my_imc(%MyIMC{} = my_imc) do
    MyIMC.changeset(my_imc, %{})
  end
end
