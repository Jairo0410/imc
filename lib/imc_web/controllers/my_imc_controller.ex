defmodule ImcWeb.MyIMCController do
  use ImcWeb, :controller

  alias Imc.Metrics
  alias Imc.Metrics.MyIMC

  action_fallback ImcWeb.FallbackController

  def index(conn, _params) do
    imcs = Metrics.list_imcs()
    render(conn, "index.html", imcs: imcs)
  end

  def create(conn, %{"my_imc" => my_imc_params}) do
    with {:ok, %MyIMC{} = my_imc} <- Metrics.create_my_imc(my_imc_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.my_imc_path(conn, :show, my_imc))
      |> render("show.json", my_imc: my_imc)
    end
  end

  def show(conn, %{"id" => id}) do
    my_imc = Metrics.get_my_imc!(id)
    render(conn, "show.json", my_imc: my_imc)
  end

  def update(conn, %{"id" => id, "my_imc" => my_imc_params}) do
    my_imc = Metrics.get_my_imc!(id)

    with {:ok, %MyIMC{} = my_imc} <- Metrics.update_my_imc(my_imc, my_imc_params) do
      render(conn, "show.json", my_imc: my_imc)
    end
  end

  def delete(conn, %{"id" => id}) do
    my_imc = Metrics.get_my_imc!(id)

    with {:ok, %MyIMC{}} <- Metrics.delete_my_imc(my_imc) do
      send_resp(conn, :no_content, "")
    end
  end
end
