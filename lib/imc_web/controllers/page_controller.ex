defmodule ImcWeb.PageController do
  use ImcWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
