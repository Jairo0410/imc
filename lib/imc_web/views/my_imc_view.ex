defmodule ImcWeb.MyIMCView do
  use ImcWeb, :view
  alias ImcWeb.MyIMCView

  def render("index.json", %{imcs: imcs}) do
    %{data: render_many(imcs, MyIMCView, "my_imc.json")}
  end

  def render("show.json", %{my_imc: my_imc}) do
    %{data: render_one(my_imc, MyIMCView, "my_imc.json")}
  end

  def render("my_imc.json", %{my_imc: my_imc}) do
    %{fecha: my_imc.fecha,
      altura_cm: my_imc.altura_cm,
      peso_lb: my_imc.peso_lb}
  end
end
