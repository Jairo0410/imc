defmodule ImcWeb.Router do
  use ImcWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ImcWeb do
    pipe_through :browser

    get "/", MyIMCController, :index
    get "/imcs/:id", MyIMCController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", ImcWeb do
  #   pipe_through :api
  # end
end
