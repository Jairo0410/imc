defmodule ImcWeb.MyIMCControllerTest do
  use ImcWeb.ConnCase

  alias Imc.Metrics
  alias Imc.Metrics.MyIMC

  @create_attrs %{
    altura_cm: 120.5,
    fecha: ~D[2010-04-17],
    peso_lb: 120.5
  }
  @update_attrs %{
    altura_cm: 456.7,
    fecha: ~D[2011-05-18],
    peso_lb: 456.7
  }
  @invalid_attrs %{altura_cm: nil, fecha: nil, peso_lb: nil}

  def fixture(:my_imc) do
    {:ok, my_imc} = Metrics.create_my_imc(@create_attrs)
    my_imc
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all imcs", %{conn: conn} do
      conn = get(conn, Routes.my_imc_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create my_imc" do
    test "renders my_imc when data is valid", %{conn: conn} do
      conn = post(conn, Routes.my_imc_path(conn, :create), my_imc: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.my_imc_path(conn, :show, id))

      assert %{
               "id" => id,
               "altura_cm" => 120.5,
               "fecha" => "2010-04-17",
               "peso_lb" => 120.5
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.my_imc_path(conn, :create), my_imc: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update my_imc" do
    setup [:create_my_imc]

    test "renders my_imc when data is valid", %{conn: conn, my_imc: %MyIMC{id: id} = my_imc} do
      conn = put(conn, Routes.my_imc_path(conn, :update, my_imc), my_imc: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.my_imc_path(conn, :show, id))

      assert %{
               "id" => id,
               "altura_cm" => 456.7,
               "fecha" => "2011-05-18",
               "peso_lb" => 456.7
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, my_imc: my_imc} do
      conn = put(conn, Routes.my_imc_path(conn, :update, my_imc), my_imc: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete my_imc" do
    setup [:create_my_imc]

    test "deletes chosen my_imc", %{conn: conn, my_imc: my_imc} do
      conn = delete(conn, Routes.my_imc_path(conn, :delete, my_imc))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.my_imc_path(conn, :show, my_imc))
      end
    end
  end

  defp create_my_imc(_) do
    my_imc = fixture(:my_imc)
    {:ok, my_imc: my_imc}
  end
end
