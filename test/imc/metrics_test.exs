defmodule Imc.MetricsTest do
  use Imc.DataCase

  alias Imc.Metrics

  describe "imcs" do
    alias Imc.Metrics.MyIMC

    @valid_attrs %{altura_cm: 120.5, fecha: ~D[2010-04-17], peso_lb: 120.5}
    @update_attrs %{altura_cm: 456.7, fecha: ~D[2011-05-18], peso_lb: 456.7}
    @invalid_attrs %{altura_cm: nil, fecha: nil, peso_lb: nil}

    def my_imc_fixture(attrs \\ %{}) do
      {:ok, my_imc} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Metrics.create_my_imc()

      my_imc
    end

    test "list_imcs/0 returns all imcs" do
      my_imc = my_imc_fixture()
      assert Metrics.list_imcs() == [my_imc]
    end

    test "get_my_imc!/1 returns the my_imc with given id" do
      my_imc = my_imc_fixture()
      assert Metrics.get_my_imc!(my_imc.id) == my_imc
    end

    test "create_my_imc/1 with valid data creates a my_imc" do
      assert {:ok, %MyIMC{} = my_imc} = Metrics.create_my_imc(@valid_attrs)
      assert my_imc.altura_cm == 120.5
      assert my_imc.fecha == ~D[2010-04-17]
      assert my_imc.peso_lb == 120.5
    end

    test "create_my_imc/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Metrics.create_my_imc(@invalid_attrs)
    end

    test "update_my_imc/2 with valid data updates the my_imc" do
      my_imc = my_imc_fixture()
      assert {:ok, %MyIMC{} = my_imc} = Metrics.update_my_imc(my_imc, @update_attrs)
      assert my_imc.altura_cm == 456.7
      assert my_imc.fecha == ~D[2011-05-18]
      assert my_imc.peso_lb == 456.7
    end

    test "update_my_imc/2 with invalid data returns error changeset" do
      my_imc = my_imc_fixture()
      assert {:error, %Ecto.Changeset{}} = Metrics.update_my_imc(my_imc, @invalid_attrs)
      assert my_imc == Metrics.get_my_imc!(my_imc.id)
    end

    test "delete_my_imc/1 deletes the my_imc" do
      my_imc = my_imc_fixture()
      assert {:ok, %MyIMC{}} = Metrics.delete_my_imc(my_imc)
      assert_raise Ecto.NoResultsError, fn -> Metrics.get_my_imc!(my_imc.id) end
    end

    test "change_my_imc/1 returns a my_imc changeset" do
      my_imc = my_imc_fixture()
      assert %Ecto.Changeset{} = Metrics.change_my_imc(my_imc)
    end
  end
end
